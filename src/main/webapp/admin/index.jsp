<%@ page session="false"%>
<!doctype html>
<html>
<head>
	<title>SECRAIDS - Edit</title>
	<meta http-equiv="X-UA-Compatible"
		content="IE=EmulateIE7, IE=EmulateIE9,chrome=1" />
	<link rel="icon" type="image/png" href="http://atacraids.com/images/favicons/bair.png" />
	<link rel="stylesheet" type="text/css" href="http://atacraids.com/css/atacraids.css" />
	<link rel="stylesheet" type="text/css"
		href="http://atacraids.com/theme/ro-gray/css/ext-all.css" />
	<link rel="stylesheet" type="text/css" href="http://atacraids.com/css/atacraids_new_headers.css" />
	
	
	<link rel="stylesheet" type="text/css"
		href="http://atacraids.com/js/ext/roweditor/css/RowEditor.css" />
	
	<link rel="stylesheet" type="text/css"
		href="http://atacraids.com/js/ext/gridfilters/css/GridFilters.css" />
	<link rel="stylesheet" type="text/css"
		href="http://atacraids.com/js/ext/gridfilters/css/RangeMenu.css" />
</head>

<body style="overflow: hidden">
	<div id="loading-mask" style=""></div>
	<div id="loading">
		<div class="loading-indicator" style="">
			<img style="float: left"
				src="http://atacraids.com/images/loading/burglary-animated_26x26.gif" alt='Loading' />
			<div style="padding-left: 32px; padding-top: 6px;">Loading...</div>
		</div>
	</div>
	<div id="contentLoading"
		style="position: absolute; top: 50%; left: 50%;">
		<span><img src="http://atacraids.com/images/loading/burglary-animated_16x16.gif"
			style="width: 16 height:            16 align:            'absmiddle'"
			alt="Loading..." /> Loading...</span>
	</div>

	<div id="headerDiv">
		<div class="head-top">
			<div class="head-top-nav">
				<div class="head-top-statement">SECRAIDS</div>
				<div id="secondarymenucontainer">
					<div id="centeredmenu">
						<ul>
							<li><a title="ATACRAIDS" href="http://atacraids.com/">App</a></li>
								
						</ul>
					</div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="head">
			<div class="head-nav">
				<div class="head-logo roc-logo" id="logoDiv"
					onclick="location.href='#'"></div>
				<div id="primarymenucontainer">
					<!-- 
					<a class="head-btn" id="nwrButton" href="#nwrButton">Apply Now</a>&nbsp; 
					<a class="head-btn" id="login-button"  href="login">Login</a>
				 -->
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>


	<div id="sidebarDiv">
		<!-- Side-bar div go here -->
		<div id="news">
		</div>
	</div>
	<div id="contentDiv">
		<!-- Content goes here -->
		<div id="registrationDivTab">
			<div id="gridDiv"></div>
		</div>
	</div>

	<div style="white-space: nowrap;" id="footerDiv">
		<div id="termsOfUse" style="float: left;">
			<a href="#" rel="external">Terms of Use</a>
		</div>
		<div style="float: right;">
			SECRAIDS is best viewed in Internet Explorer 9+, Firefox and
			Chrome.&nbsp;&nbsp;|&nbsp;&nbsp;<b>Powered by <img
				src="http://atacraids.com/images/favicons/bair.png" alt="Bair"
				style="width: 12px; height: 12px; vertical-align: middle" /> <a
				href="http://www.bairanalytics.com/">BAIR Analytics</a></b>
		</div>
	</div>

	<script type="text/javascript" src="http://atacraids.com/js/jquery/jquery-1.7.1.min.js"></script>
	<script type="text/javascript"
		src="http://atacraids.com/js/jquery/jquery-ui-1.7.2.custom.min.js"></script>

	<script type="text/javascript"
		src="http://atacraids.com/js/ext-3.4.0/adapter/jquery/ext-jquery-adapter.js"></script>
	<script type="text/javascript" src="http://atacraids.com/js/ext-3.4.0/ext-all.js"></script>
	
		
	<script type="text/javascript" src="http://atacraids.com/js/ext/roweditor/RowEditor.js"></script>
	<script type="text/javascript"
		src="http://atacraids.com/js/ext/gridfilters/menu/RangeMenu.js"></script>
	<script type="text/javascript"
		src="http://atacraids.com/js/ext/gridfilters/menu/ListMenu.js"></script>
	
	<script type="text/javascript"
		src="http://atacraids.com/js/ext/gridfilters/GridFilters.js"></script>
	<script type="text/javascript"
		src="http://atacraids.com/js/ext/gridfilters/filter/Filter.js"></script>
	<script type="text/javascript"
		src="http://atacraids.com/js/ext/gridfilters/filter/StringFilter.js"></script>
	<script type="text/javascript"
		src="http://atacraids.com/js/ext/gridfilters/filter/DateFilter.js"></script>
	<script type="text/javascript"
		src="http://atacraids.com/js/ext/gridfilters/filter/ListFilter.js"></script>
	<script type="text/javascript"
		src="http://atacraids.com/js/ext/gridfilters/filter/NumericFilter.js"></script>
	<script type="text/javascript"
		src="http://atacraids.com/js/ext/gridfilters/filter/BooleanFilter.js"></script>
	
	<script type="text/javascript" src="http://atacraids.com/js/ext/ux/CheckColumn.js"></script>
	<script type="text/javascript" src="http://atacraids.com/js/ext/ux/XDateField.js"></script>
	<script type="text/javascript" src="http://atacraids.com/js/ext/ux/EnsureVisible.js"></script>		

	<script type="text/javascript" src="../js/secraids.js"></script>
	<script type="text/javascript" src="../js/layout.js"></script>	

</body>
</html>