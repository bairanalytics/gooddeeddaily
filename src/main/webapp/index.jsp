<html>
<head>
<link rel="stylesheet" type="text/css"
	href="http://raidsonline.com/theme/ro-gray/css/ext-all.css">
<script type="text/javascript"
	src="http://raidsonline.com/js/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript"
	src="http://raidsonline.com/js/ext-3.4.0/adapter/ext/ext-base.js"></script>
<script type="text/javascript"
	src="http://raidsonline.com/js/ext-3.4.0/ext-all.js"></script>
<link rel='stylesheet' type='text/css' href='http://raidsonline.com/css/atacraids-extjs.css' />

<script type='text/javascript' src='http://raidsonline.com/js/ext/ux/Utilities.js' ></script>

<script type="text/javascript">
	function showMessage(title, message, onOk) {

		return Ext.MessageBox.show({
			title : title,
			msg : message,
			fn : onOk,
			buttons : Ext.MessageBox.OK,
			icon : Ext.MessageBox.INFO
		});

	}
	function GoodDeedDaily() {
		this.gddSignupWindow = null;
		var gdd = this;
		var e = (6 - ((new Date()).getTimezoneOffset() / 60)) * 100;
		var x = 0.0, y = 0.0;
		var frm = new Ext.FormPanel(
				{
					labelWidth : 85,
					frame : false,
					border : false,
					region : "center",
					closable : true,
					bodyStyle : "padding: 4px 4px 4px 8px;",
					defaults : {
						width : 200
					},
					defaultType : "textfield",
					items : [ { 
					    xtype: "label",
					    style: 'font-weight: bold;',
					    text: "Why only look at the negatives? Enter a good deed such as a service project, feeding the homeless, carrying groceries, etc. Thanks for adding some positivity to RaidsOnline!"
					}, {
						xtype: "displayfield",
						height: 5
					}, {
						xtype: "label",
						style: 'font-weight: bold;',
						text: "Enter your personal information below: "
					}, {
						xtype: "displayfield",
					}, {
						fieldLabel: "First Name",
						name: "first",
						allowBlank: false
					}, {
						fieldLabel: "Last Name",
						name: "last",
						allowBlank: false
					}, {
						fieldLabel: "Email",
						name: "email",
						allowBlank: false
					}, {
						xtype: "numberfield",
						fieldLabel: "Phone Number",
						name: "phone",
						allowBlank: false,
						allowDecimals: false,
						minLength: 10,
						maxLength: 10
					}, {
						xtype: "displayfield",
						height: 10
					}, {
						xtype: "label",
						style: 'font-weight: bold;',
						text: "Enter information about the Good Deed below: "
					}, {
						xtype: "displayfield",
						height: 5
					}, {
						fieldLabel : "Who",
						name : "who",
						allowBlank : false
					}, {
						xtype : "datefield",
						fieldLabel : "Date",
						name : "when",
						id : "gddDate",
						allowBlank : false
					}, {
						fieldLabel : "Address",
						name : "where",
						allowBlank : false
					}, {
						xtype : "textarea",
						fieldLabel : "Good Deed",
						name : "what",
						allowBlank : false
					} ],
					buttons : [
							{
								text : "Submit",
								handler : function() {
									frm
											.getForm()
											.submit(
													{
														clientValidation : true,
														url : "rest/crud/form",
														success : function(m, n) {
															showMessage(
																	n.result.title,
																	n.result.msg);
															this.gddSignupWindow
																	.hide()
														},
														failure : function(m, n) {
															switch (n.failureType) {
															case Ext.form.Action.CLIENT_INVALID:
																Ext.Msg
																		.alert(
																				"Invalid Input",
																				"Please hover your mouse over the red underlines to learn more about the problem.");
																break;
															case Ext.form.Action.CONNECT_FAILURE:
																showMessage(
																		"Connection Failure",
																		"Unable to connect to server.");
																break;
															case Ext.form.Action.SERVER_INVALID:
																showMessage(
																		n.result.title,
																		n.result.message)
															}
														}
													})
								}
							}, {
								text : "Cancel",
								handler : function() {
									this.gddSignupWindow.hide()
								}
							} ]
				});

		this.gddSignupWindow = new Ext.Window({
			title : "Submit a Good Deed",
			layout : "border",
			width : 320,
			height : 448,
			plain : true,
			items : [ frm ],
			border : false,
			modal : true,
			closeAction : "hide"
		});
		this.gddSignupWindow.render(document.body)
	}
	var goodDeedDaily;
	$(document).ready(function() {
		goodDeedDaily = new GoodDeedDaily();
		goodDeedDaily.gddSignupWindow.show(this);
	})
</script>

</head>
<body>
	<!-- <a class='head-btn' id='gddButton' href='#gddButton'>Submit a Good
		Deed</a> -->




</body>
</html>
