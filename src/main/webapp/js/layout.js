function showMessage(title, message) {

    Ext.MessageBox.show({
        title: title,
        msg: message,
        buttons: Ext.MessageBox.OK,
        icon: Ext.MessageBox.INFO
    });

}
//var LAYOUT = LAYOUT || {
//	sidebarItems : [],
//	tabItems : []
//};
Ext.onReady(function(){
	
	Ext.QuickTips.init();
	
	/* Header */
	var headerPanel = {
			region: 'north',
			height: 85,
			border: false,
			contentEl: 'headerDiv'
	}; 

	/* Content */
	var contentPanel = {
			region: 'center',
			border: false,
	        layout: 'fit',
			bodyStyle: 'background-color: #444b4d;',
			items : [],
			contentEl: 'contentDiv'
	};
	if(LAYOUT.tabItems){
		/* Tabs */
		contentPanel = new Ext.TabPanel({
	    	id: 'tabs',
	    	activeTab: 0,
	    	region : 'center',
			defaults : {
				autoWidth : true,
	        	autoScroll: true,
				padding : '10px'

			},
	        items:[LAYOUT.tabItems],
	        listeners : LAYOUT.tabListeners || {}
	    });
	    // Add to Content Panel
	}
	/* Footer */
	var footerPanel = {
			id: 'foot',
			region: 'south',
			height: 25,
			border: false,
			bodyStyle: 'background-color: #191b1d; color: #fff; padding: 4px; align: right',
			contentEl: 'footerDiv'
	};
	var appItems = [headerPanel, contentPanel,footerPanel];
	
	if(LAYOUT.westSideBarPanel){
		appItems.push(LAYOUT.westSideBarPanel); 
	}
	if(LAYOUT.sidebarItems){
		appItems.push({
			id : 'sideBarPanel-extjs',
			title : 'Menu',
			region : 'west',
			collapsible : true,
			width : 265,
			autoScroll : true,
			defaults : {
				autoWidth : true,
				collapsible : true,
				collapsed : false,
				collapsedCls : 'collapsedMenubox',
				bodyCssClass : 'menubox'
			},
			items : LAYOUT.sidebarItems,
			bodyStyle : 'background-color: #eaeaea; padding: 4px',
			contentEl : 'sidebarDiv'
		})
	}
	
	/* Layout */
	var appLayout = new Ext.Viewport({
			layout: 'border',
			items: appItems
	});
	
	/* Remove Loading Mask */
	$("#contentLoading").fadeOut("fast");
    Ext.get('loading').remove();
    Ext.get('loading-mask').fadeOut({remove:true});
	
});


/* Helper Methods */

function getGridPanelHeight() {
	return Ext.get('tabs').getHeight() - 30;
}

/* Loading Box */
var myMask = new Ext.LoadMask(Ext.getBody(), {
	msg : "Please wait..."
});
var loading = false;
function startLoading() {
	loading = true;
	myMask.show();

}

function stopLoading() {
	loading = false;
	myMask.hide();
}
var window_beforeUnload = function(e) {
	if (loading) {
		var message = 'Leaving this page before the request completes may result in temporary inconsistencies. ';

		e = e || e.event;

		if (e) {
			e.returnValue = message;
		}

		return message;
	}
};
if (window.addEventListener) {
	window.addEventListener('beforeunload', window_beforeUnload, false);
} else if (window.attachEvent) {
	window.attachEvent('onbeforeunload', window_beforeUnload);
}
