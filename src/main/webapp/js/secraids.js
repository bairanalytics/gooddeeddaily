
function SecraidsCrud(params) {
	params = params || {};
	var model = {};
	var loadMask = new Ext.LoadMask(Ext.getBody(), {msg:'Loading...'});

	var verifyId = function() {
		var id = model.selectedId;
		var record = model.record;
		if (record == null || record == undefined) {
			setTimeout(function() {
				showMessage('User Input', 'Please select an incident to manage.');
			}, 250);
			return false;
		}
		return true;
	};
	var classStore = new Ext.data.ArrayStore({
		fields : [ 'name', 'value' ],
		data : [ [ 'Extortion', 3001 ], [ 'Assassinations', 3000 ],
				[ 'Gang-on-gang', 3003 ], [ 'Gang-on-public', 3004 ],
				[ 'Gang-on-officer', 3005 ],['Kidnappings',3002]]
	});
	var accStore = new Ext.data.ArrayStore({
		fields : [ 'name', 'value' ],
		data : [ [ 'Address', 8 ], [ 'Street', 7 ],
				[ 'Intersection', 5 ] ]
	});
	var config = [ {
		hidden : false,
		header : 'Incident ID',
		group : 'General',
		allowBlank : false,
		sortable : true,
		dataIndex : 'incidentId',
		editor : {
			xtype : 'textfield'
		}
	}, {
		header : 'Class',
		sortable : true,
		dataIndex : 'classId',
		editor : {
			xtype : 'combo',
			typeAhead : true,
			allowBlank : false,
			triggerAction : 'all',
			lazyRender : false,
			mode : 'local',
			store : classStore,
			displayField : 'name',
			valueField : 'value'
		},
		group : 'General',
		renderer : function(val){
			if (val != null) {
				var idx = classStore.find('value', val);
				if (idx != null && idx > -1) {
					return classStore.getAt(idx).data.name;
				}
			}
			return '';
		}

	}, {
		header : 'Date',
		allowBlank : false,
		sortable : true,
		dataIndex : 'datetime',
		editor : {
			xtype : 'xdatefield',
			submitFormat : 'U000'
		},
		group : 'General',
		renderer : function(value) {
			if (value) {
				var d = value;
				if (!Ext.isDate(value)) {
					d = new Date(value);
					
				}
				return d.format('m-d-Y');					
			}
			return value;
		}
	},{
		header : 'Time',
		allowBlank : false,
		sortable : false,
		dataIndex : 'time',
		group : 'General',
		editor : {
			xtype : 'timefield'
		}
	}, {
		header : 'Crime',
		sortable : true,
		dataIndex : 'crime',
		editor : {
			xtype : 'textfield'
		},
		group : 'General'
	}, {
		sortable : true,
		header : 'Location Type',
		group : 'General',
		dataIndex : 'locationType',
		editor : {
			xtype : 'textfield'
		}
	}, {
		header : 'Address',
		sortable : true,
		dataIndex : 'address',
		editor : {
			xtype : 'textfield'
		},
		group : 'General'
	}, {
		header : 'X/Longitude',
		allowBlank : true,
		sortable : true,
		dataIndex : 'x',
		editor : {
			xtype : 'numberfield',
			allowDecimals : true,
			decimalPrecision : 8
		},
		group : 'General',
	    validator: function(value){
	        if(value < -180 || value > 180) {
	            return 'Error! X Coodinate must be between -180 and 180 degrees longitude';
	        } else {
	            return true;
	        }
	    }
	}, {
		header : 'Y/Latitude',
		allowBlank : true,
		sortable : true,
		dataIndex : 'y',
		editor : {
			xtype : 'numberfield',
			allowDecimals : true,
			decimalPrecision : 8
		},
		group : 'General',
	    validator: function(value){
	        if(value < -90 || value > 90) {
	            return 'Error! Y Coodinate must be between -90 and 90 degrees longitude';
	        } else {
	            return true;
	        }
	    }

	}, {
		header : 'Accuracy',
		sortable : true,
		allowBlank : false,
		dataIndex : 'accuracyId',
		editor : {
			xtype : 'combo',
			typeAhead : true,
			triggerAction : 'all',
			lazyRender : true,
			mode : 'local',
			store : accStore,
			displayField : 'name',
			valueField : 'value'

		},
		group : 'General',
		renderer : function(val){
			if (val != null) {
				if (val == 0) {
					return;
				}
				var idx = accStore.find('value', val);
				if (idx != null && idx > -1) {
					return accStore.getAt(idx).data.name;
				}
			}
			return '';
		}
	}, {
		header : 'Narrative',
		allowBlank : true,
		height : 250,
		dataIndex : 'narrative',
		editor : {
			xtype : 'textarea'
		},
		group : 'Narrative',
		renderer : function(value, cell, model, index) {
			if (!value)
				return value;
			if (value.length > 50)
				return value.substring(0, 50) + '...';
			return value;
		}
	} ];

	// Populate the name
	$.each(config, function(i, val) {
		val.name = val.dataIndex;
	});
	var gridColumns = [];
	// Copy the configs into the gridColumns
	$.each(config, function(k, v) {
		var n = {};
		$.extend(n, v);
		if(v.gridRenderer){
			n.renderer = v.gridRenderer;
		}
		gridColumns.push(n);
	});

	function IncidentForm(theRecord, theStore) {
		var fromColumns = function(group) {
			var arr = [];
			$.each(config, function(i, val) {
				if (val.group == group) {
					var cfg = {};
					$.extend(cfg, val);
					cfg.fieldLabel = cfg.header;
					cfg.header = null;
					cfg.hidden = false;
					if (val.editable == false)
						cfg.disabled = true;
					if (val.editor) {
						$.extend(cfg, cfg.editor);
					}
					arr.push(cfg);
				}
			});
			return arr;
		};
		var form = new Ext.FormPanel({
			layout : 'accordion',
			defaults : {
				collapsed : true,
				layout : 'form',
				padding : '10px',
				hideMode : 'offsets',
				autoScroll : true
			},
			items : [ {
				title : 'General',
				collapsed : false,
				items : fromColumns('General')
			}, {
				title : 'Narrative',
				collapsed : false,
				layout : {
					type : 'vbox',
					align : 'stretch' // Child items are stretched to full
				// width
				},
				items : fromColumns('Narrative')
			} ]
		});
		if (theRecord) {
			form.getForm().loadRecord(theRecord);
		}
		var formWindow = null;
		var closeHandler = function(doClose) {
			if (theRecord.phantom) {
				theStore.remove(theRecord);
			} else if (theRecord.dirty) {
				Ext.Msg.confirm('Confirm', 'Unsaved changes will be lost?',
						function(btn, text) {
							if (btn == 'yes') {
								formWindow.close();
							}
						});
				return false;
			}
			if (doClose) {
				formWindow.close();
			}
			return true;
		};
		formWindow = new Ext.Window({
			title : 'Incident Builder',
			layout : 'fit',
			id : 'formwindow',
			width : 430,
			height : 400,
			modal : true,
			plain : true,
			maximizable : true,
			items : form,
			listeners : {
				closebefore : closeHandler
			},
			buttons : [ {
				text : 'Submit',
				handler : function() {
					var f = form.getForm();
					if (f.isValid()) {
						f.updateRecord(theRecord);
						if (theRecord.dirty == false) {
							showMessage('Submit', 'Nothing to update');
						}else{
							loadMask.show();
						}
					} else {
						var invalid = f.findInvalid()[0];
						if (invalid) {
							invalid.ensureVisible();
						}
					}

				}
			}, {
				text : 'Close',
				handler : function() {
					closeHandler(true);
				}
			} ]
		});

		return {
			window : formWindow,
			form : form
		};
	}

	var gridStore = new Ext.data.JsonStore({
		root : 'data',
		autoSave : true,
		fields : gridColumns,
		idProperty : 'id',
		messageProperty : 'message',
		// sortInfo: { field: 'name', direction: 'DESC' },
		listeners : {
			beforeload : function(store) {
				var form = Ext.getCmp('searchForm').getForm();
				var values = form.getFieldValues();
				store.setBaseParam('incidentId',values.searchId);
			},
			write : function(store, action, result, res, rs) {
				loadMask.hide();
				var theWindow = Ext.getCmp('formwindow');
				if (theWindow) {
					theWindow.close();
				}
			},
			exception : function(proxy, type, action, options, response, arg) {
				loadMask.hide();
				showMessage('Error', 'Server-Side error: ' + response.message);
			}
		},
		proxy : new Ext.data.HttpProxy({

			restful : true,
			api : {
				read : '../rest/crud/secraids/database',
				create : {
					url : '../rest/crud/secraids/database/add',
					method : 'POST'
				},
				update : {
					url : '../rest/crud/secraids/database/update',
					method : 'POST'
				},
				destroy : '../rest/crud/secraids/database/delete'
			}
		}),
		writer : new Ext.data.JsonWriter({
			encode : false,
			writeAllFields : true,
			render : function(params, baseParams, data) {
				Ext.apply(params, baseParams);
				Ext.apply(params, data);
			}
		})

	});

	var pagingToolbar = new Ext.PagingToolbar({
		pageSize : 25,
		store : gridStore,
		displayInfo : true,
		displayMsg : 'Displaying records {0} - {1} of {2}',
		emptyMsg : "No records in view",
		listeners : {}
	});
	// Ext.grid.GridPanel Ext.grid.EditorGridPanel
	var grid = null;
	grid = new Ext.grid.GridPanel({
		id : 'incidentGridPanel',
		enableColumnMove : false,
		columns : gridColumns,
		sm : new Ext.grid.RowSelectionModel({
			singleSelect : true,
			listeners : {
				scope : this,
				rowselect : function(sm, rowIndex, record) {
					model.selectedId = record.data.id;
					model.record = record;
					if (params.rowselect) {
						params.rowselect(sm, rowIndex, record);
					}
				}
			}
		}),
		defaults : {
			sortable : true,
			menuDisabled : true,
			enableColumnMove : false,
			width : 100
		},
		viewConfig : {
			forceFit : false
		},
		store : gridStore,
		columnLines : true,
		stripeRows : true,
		autoScroll : true,
		forceLayout : true,
		clicksToEdit : 1,
		listeners : {
			'edit' : function(editor, e) {
				editor.record.commit();
			}
		},
		tbar : [
				{
					xtype : 'button',
					text : 'New Incident',
					handler : function() {
						var theRecord = new gridStore.recordType({
							id : null
						}, 0);
						gridStore.insert(0, theRecord);
						theRecord.phantom = true; // indicates a new
						// record
						var e = new IncidentForm(theRecord, gridStore);
						e.window.show();
					}
				},
				{
					xtype : 'button',
					text : 'Edit Incident',
					handler : function() {
						if (verifyId()) {
							var e = new IncidentForm(model.record);
							e.window.show();
						}
					}
				},
				{
					xtype : 'button',
					text : 'Delete Incident',
					handler : function() {
						if (verifyId()) {
							Ext.MessageBox.confirm('Delete', 'Are you sure ?',
									function(btn) {
										if (btn === 'yes') {
											gridStore.remove(model.record);
										}
									});
						}
					}
				},'|',
				{
					xtype : 'button',
					text : 'Jump to Incident',
					handler : function() {
						if (verifyId()) {
							var record = model.record.data;
							var address = '('+record.y+','+record.x+')';
							var crime = record.classId;
							var start = record.datetime;
							if(typeof start != 'string'){
								var d = new Date(start);
								start = d.format('Y-m-d');
							}
							var url = '../?icon=off&zoom=12&address='+address+'&crimeTypes=['+crime
								+']&startDate='+start+'&endDate='+start;
							window.open(url, "_blank");
						}
					}
				} ],
		bbar : pagingToolbar
	});
	var formHandler = function(btn, evt) {

		gridStore.load();
	};
	var searchVal = Ext.urlDecode(location.search.substring(1)).id;
	if(searchVal){
		Ext.onReady(function(){
			formHandler();
		});
	}
	// Create side bar
	var sidebar = new Ext.FormPanel(
			{
				title : 'Search Form',
				id : 'searchForm',
				collapsed : true,
				items : [
						{
							xtype : 'textfield',
							name : 'searchId',
							fieldLabel : 'Incident Id',
							value : searchVal
						},
						{
							text : 'Search',
							xtype : 'button',
							id : 'incidentSearchButton',
							handler : formHandler
						} ]
			});
	return {
		grid : grid,
		store : gridStore,
		model : model,
		verifyId : verifyId,
		sidebar : sidebar,
		panel : {
			title : 'Incidents',
			bodyStyle : 'background-color: #dfdfdf;',
			padding : '0px',
			layout : 'fit',
			items : grid
		}
	};
}
var LAYOUT = LAYOUT || {
	sidebarItems : [],
	tabItems : []
};
try {
	var crud = new SecraidsCrud();

	LAYOUT.tabItems.push(crud.panel);
	LAYOUT.sidebarItems.push(crud.sidebar);
} catch (e) {
	console.log(e);
}