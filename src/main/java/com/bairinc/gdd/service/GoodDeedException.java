package com.bairinc.gdd.service;

public class GoodDeedException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public GoodDeedException() {
	}

	public GoodDeedException(String message) {
		super(message);
	}

	public GoodDeedException(Throwable cause) {
		super(cause);
	}

	public GoodDeedException(String message, Throwable cause) {
		super(message, cause);
	}
}

/*
 * Location: C:\Users\fivekiller\Dropbox (BAIR
 * Analytics)\Servers\Deployment\GoodDeedDaily\GoodDeedDaily - Copy.zip
 * 
 * Qualified Name: WEB-INF.classes.com.bairinc.gdd.service.GoodDeedException
 * 
 * JD-Core Version: 0.7.0.1
 */