package com.bairinc.gdd.service;

import com.bairinc.gdd.GoodDeed;

public abstract interface GoodDeedRepository
{
  public abstract GoodDeed create(GoodDeed paramGoodDeed);
  
  public abstract GoodDeed update(GoodDeed paramGoodDeed);
  
  public abstract GoodDeed read(int paramInt);
  
  public abstract boolean delete(GoodDeed paramGoodDeed);
}
