package com.bairinc.gdd.service;

import com.bairinc.gdd.GoodDeed;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GoodDeedRepositoryImpl implements GoodDeedRepository {
	private static final Logger log = LoggerFactory
			.getLogger(GoodDeedRepositoryImpl.class);
	private final DataSource source;
	private final QueryRunner db;

	public GoodDeedRepositoryImpl(DataSource source) {
		this.source = source;
		this.db = new QueryRunner(source);
	}

	public GoodDeed create(GoodDeed gd) {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			conn = this.source.getConnection();
			stmt = conn
					.prepareStatement("INSERT INTO good_deed (who,what,[when],[where],x,y) VALUES (?,?,?,?,?,?);\nSELECT SCOPE_IDENTITY();");

			stmt.setString(1, gd.getWho());
			stmt.setString(2, gd.getWhat());
			stmt.setString(3, gd.getWhen());
			stmt.setString(4, gd.getWhere());
			stmt.setDouble(5, gd.getX());
			stmt.setDouble(6, gd.getY());
			int execute = stmt.executeUpdate();
			if (execute > 0) {
				rs = stmt.getGeneratedKeys();
				if (rs.next()) {
					int id = rs.getInt(1);
					gd.setId(id);
					return gd;
				}
			}
			throw new GoodDeedException("Could not get good deed id: " + gd);
		} catch (SQLException e) {
			log.error("Could not execute create: " + gd, e);
			throw new GoodDeedException(e);
		} finally {
			DbUtils.closeQuietly(conn, stmt, rs);
		}
	}

	private Object[] toParams(GoodDeed gd) {
		return new Object[] { gd.getWho(), gd.getWhat(), gd.getWhen(),
				gd.getWhere(), Double.valueOf(gd.getX()),
				Double.valueOf(gd.getY()), Integer.valueOf(gd.getId()) };
	}

	public GoodDeed update(GoodDeed gd) {
		try {
			int rows = this.db
					.update("UPDATE good_deed SET who=?, what=?, [when]=?, [where]=?, x=?, y=? WHERE id=?",
							toParams(gd));
			if (rows == 0) {
				throw new GoodDeedException("Invalid Good Deed ID: "
						+ gd.getId());
			}
		} catch (SQLException e) {
			log.error("Could not execute update: " + gd, e);
			throw new GoodDeedException(e);
		}
		return gd;
	}

	public GoodDeed read(int id) {
		ResultSetHandler<GoodDeed> rsh = new BeanHandler<GoodDeed>(GoodDeed.class);
		GoodDeed goodDeed = null;
		try {
			goodDeed = this.db.query(
					"SELECT * FROM good_deed where id =?", rsh,
					new Object[] { Integer.valueOf(id) });
		} catch (SQLException e) {
			log.error("Could not execute read: " + id, e);
			throw new GoodDeedException(e);
		}
		return goodDeed;
	}

	public boolean delete(GoodDeed gd) {
		try {
			int rows = this.db.update("DELETE FROM good_deed WHERE id=?",
					Integer.valueOf(gd.getId()));
			return rows != 0;
		} catch (SQLException e) {
			log.error("Could not execute update: " + gd, e);
			throw new GoodDeedException(e);
		}
	}
}