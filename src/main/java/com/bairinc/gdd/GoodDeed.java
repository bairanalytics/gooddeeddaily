package com.bairinc.gdd;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class GoodDeed {
	private int id = -1;
	private String who;
	private String what;
	private String when;
	private String where;
	private double x = 0.0D;
	private double y = 0.0D;
	private String first_name;
	private String last_name;
	private String email;
	private long phone;

	public GoodDeed() {
	}

	public GoodDeed(GoodDeed gd) {
		this.who = gd.who;
		this.what = gd.what;
		this.when = gd.when;
		this.where = gd.where;
		this.x = gd.x;
		this.y = gd.y;
		this.first_name = gd.first_name;
		this.last_name = gd.last_name;
		this.email = gd.email;
		this.phone = gd.phone;
	}

	public GoodDeed(String who, String what, String when, String where,
			double x, double y, String first, String last, String email, long phone) {
		this(-1, who, what, when, where, x, y, first, last, email, phone);
	}
	
	public GoodDeed(String who, String what, String when, String where, String first, String last, String email, long phone) {
		this(who, what, when, where, 0.0, 0.0, first, last, email, phone);
	}

	public GoodDeed(int id, String who, String what, String when, String where,
			double x, double y, String first, String last, String email, long phone) {
		this.id = id;
		this.who = who;
		this.what = what;
		this.when = when;
		this.where = where;
		this.x = x;
		this.y = y;
		this.first_name = first;
		this.last_name = last;
		this.email = email;
		this.phone = phone;
	}

	public final int getId() {
		return this.id;
	}

	public final void setId(int id) {
		this.id = id;
	}

	public final String getWho() {
		return this.who;
	}

	public final void setWho(String who) {
		this.who = who;
	}

	public final String getWhat() {
		return this.what;
	}

	public final void setWhat(String what) {
		this.what = what;
	}

	public final String getWhen() {
		return this.when;
	}

	public final void setWhen(String when) {
		this.when = when;
	}

	public final String getWhere() {
		return this.where;
	}

	public final void setWhere(String where) {
		this.where = where;
	}

	public final double getX() {
		return this.x;
	}

	public final void setX(double x) {
		this.x = x;
	}

	public final double getY() {
		return this.y;
	}

	public final void setY(double y) {
		this.y = y;
	}
	
	public final String getFirst() {
		return this.first_name;
	}
	
	public final void setFirst(String first) {
		this.first_name = first;
	}
	
	public final String getLast() {
		return this.last_name;
	}
	
	public final void setLast(String last) {
		this.last_name = last;
	}
	
	public final String getEmail() {
		return this.email;
	}
	
	public final void setEmail(String email) {
		this.email = email;
	}
	
	public final long getPhone() {
		return this.phone;
	}
	
	public final void setPhone(long phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return

		"GoodDeed [id=" + this.id + ", who=" + this.who + ", what=" + this.what
				+ ", when=" + this.when + ", where=" + this.where + ", x="
				+ this.x + ", y=" + this.y + ", first=" + this.first_name + ", last=" + this.last_name + ", email=" + this.email + ", phone=" + this.phone + "]";
	}

	@Override
	public int hashCode() {
		int result = 1;
		result = 31 * result + this.id;
		result = 31 * result + (this.what == null ? 0 : this.what.hashCode());
		result = 31 * result + (this.when == null ? 0 : this.when.hashCode());
		result = 31 * result + (this.where == null ? 0 : this.where.hashCode());
		result = 31 * result + (this.who == null ? 0 : this.who.hashCode());

		long temp = Double.doubleToLongBits(this.x);
		result = 31 * result + (int) (temp ^ temp >>> 32);
		temp = Double.doubleToLongBits(this.y);
		result = 31 * result + (int) (temp ^ temp >>> 32);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		GoodDeed other = (GoodDeed) obj;
		if (this.id != other.id) {
			return false;
		}
		if (this.what == null) {
			if (other.what != null) {
				return false;
			}
		} else if (!this.what.equals(other.what)) {
			return false;
		}
		if (this.when == null) {
			if (other.when != null) {
				return false;
			}
		} else if (!this.when.equals(other.when)) {
			return false;
		}
		if (this.where == null) {
			if (other.where != null) {
				return false;
			}
		} else if (!this.where.equals(other.where)) {
			return false;
		}
		if (this.who == null) {
			if (other.who != null) {
				return false;
			}
		} else if (!this.who.equals(other.who)) {
			return false;
		}
		if (Double.doubleToLongBits(this.x) != Double.doubleToLongBits(other.x)) {
			return false;
		}
		if (Double.doubleToLongBits(this.y) != Double.doubleToLongBits(other.y)) {
			return false;
		}
		return true;
	}
}

/*
 * Location: C:\Users\fivekiller\Dropbox (BAIR
 * Analytics)\Servers\Deployment\GoodDeedDaily\GoodDeedDaily - Copy.zip
 * 
 * Qualified Name: WEB-INF.classes.com.bairinc.gdd.GoodDeed
 * 
 * JD-Core Version: 0.7.0.1
 */