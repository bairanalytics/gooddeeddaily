package com.bairinc.gdd.rest;

import com.bairinc.extjs.ExtjsResponse;
import com.bairinc.gdd.GoodDeed;
import com.bairinc.gdd.service.GoodDeedRepository;
import com.bairinc.gdd.service.GoodDeedRepositoryImpl;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/crud")
public class GoodDeedCrudService {
	private static final Logger log = LoggerFactory
			.getLogger(GoodDeedCrudService.class);
	private GoodDeedRepository repo;

	public GoodDeedCrudService() {
		this.repo = new GoodDeedRepositoryImpl(getDataSource());
	}

	private static DataSource getDataSource() {
		String resource = "java:comp/env/jdbc/Microsoft";
		try {
			InitialContext initialContext = new InitialContext();
			return (DataSource) initialContext.lookup(resource);
		} catch (NamingException e) {
			log.error("Error during lookup", e);

			throw new RuntimeException("Could not load ddatasource: "
					+ resource);
		}
	}

	@POST
	@Path("/form")
	@Produces({ "application/json","application/xml" })
	public ExtjsResponse create(@FormParam("who") String who,
			@FormParam("what") String what, @FormParam("where") String where,
			@FormParam("when") String when, @FormParam("first") String first, @FormParam("last") String last, @FormParam("email") String email, @FormParam("phone") long phone) {
		System.out.println("gdd->create() who: " + who + " what: " + what
				+ " when: " + when + " where: " + where + " first name: " + first + " last name: " + last + " email: " + email + " phone: " + phone);
		GoodDeed gd = new GoodDeed(who, what, when, where, first, last, email, phone);
		gd = this.repo.create(gd);
		System.out.println(gd.getId());
		return new ExtjsResponse(true, "Success", "Good Deed entered.");
	}

	@POST
	@Produces({ "application/json","application/xml" })
	@Consumes({ "application/json" })
	public GoodDeed create(GoodDeed gd) {
		gd = this.repo.create(gd);
		System.out.println("ID: " + gd.getId() + "\nWho: " + gd.getWho() + "\nWhat: " + gd.getWhat() + "\nWhen: " + gd.getWhen() + "\nWhere: " + gd.getWhere() + "\nFirst: " + gd.getFirst() + "\nLast: " + gd.getLast() + "\nEmail: " + gd.getEmail() + "\nPhone: " + gd.getPhone());
		return gd;
	}

	@PUT
	@Produces({ "application/json" })
	@Consumes({ "application/json" })
	public GoodDeed update(GoodDeed gd) {
		return this.repo.update(gd);
	}

	@GET
	@Path("/{id}")
	@Produces({ "application/json" })
	public GoodDeed read(@PathParam("id") int id) {
		return this.repo.read(id);
	}

	@DELETE
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response delete(GoodDeed gd) {
		boolean result = this.repo.delete(gd);
		if (result) {
			return Response.ok().build();
		}
		return Response.serverError().build();
	}

	@GET
	@Path("/sample")
	@Produces({ "application/json" })
	public GoodDeed sample() {
		return new GoodDeed(-1, "John Doe", "This is what they did.",
				"07/14/2014 11:00am", "123 Main St, Denver, CO", 39.0D, -156.0D, "Jane", "Doe", "example@sample.com", 1234567890);
	}
}
