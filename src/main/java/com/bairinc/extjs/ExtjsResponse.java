package com.bairinc.extjs;

public class ExtjsResponse {

	private boolean success;
	private String title;
	private String msg;
	
	public ExtjsResponse() {
		
	}

	public ExtjsResponse(boolean success, String title, String msg) {
		super();
		this.success = success;
		this.title = title;
		this.msg = msg;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
