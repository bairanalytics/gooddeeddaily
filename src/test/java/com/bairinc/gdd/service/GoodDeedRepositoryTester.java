package com.bairinc.gdd.service;

import com.bairinc.gdd.GoodDeed;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.beans.PropertyVetoException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class GoodDeedRepositoryTester {
	private static ComboPooledDataSource ds;
	private static GoodDeedRepositoryImpl repo;

	@BeforeClass
	public static void beforeClass() throws PropertyVetoException {
		String conn = "jdbc:sqlserver://";
		String ip = "192.168.20.132";
		int port = 1433;
		String userName = "test";
		String userPswd = "test";
		ds = new ComboPooledDataSource();
		ds.setDriverClass("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		ds.setJdbcUrl(conn + ip + ":" + port + ";" + "databaseName=" + "Event");
		ds.setDescription("MS SQLServerDataSource");
		ds.setUser(userName);
		ds.setPassword(userPswd);

		repo = new GoodDeedRepositoryImpl(ds);
	}

	@AfterClass
	public static void tearDown() throws Exception {
		if (ds != null) {
			ds.close();
		}
	}

	@Test
	public void testCreate() {
		GoodDeed result = repo.create(new GoodDeed("John Doe",
				"This is what they did.", "07/14/2014 11:00am",
				"123 Main St, Denver, CO", 39.0D, -156.0D, "Jane", "Doe", "example@sample.com", 1234567890));
		Assert.assertNotNull(result);
	}

	@Test
	public void testUpdate() {
		GoodDeed created = repo.create(new GoodDeed("John Doe",
				"This is what they did.", "07/14/2014 11:00am",
				"123 Main St, Denver, CO", 39.0D, -156.0D, "Jane", "Doe", "example@sample.com", 1234567890));
		GoodDeed result = repo.update(created);
		Assert.assertNotNull(result);
	}

	@Test
	public void testRead() {
		GoodDeed created = repo.create(new GoodDeed("John Doe",
				"This is what they did.", "07/14/2014 11:00am",
				"123 Main St, Denver, CO", 39.0D, -156.0D, "Jane", "Doe", "example@sample.com", 1234567890));

		GoodDeed result = repo.read(created.getId());
		Assert.assertNotNull(result);
	}

	@Test
	public void testDelete() {
		GoodDeed created = repo.create(new GoodDeed("John Doe",
				"This is what they did.", "07/14/2014 11:00am",
				"123 Main St, Denver, CO", 39.0D, -156.0D, "Jane", "Doe", "example@sample.com", 1234567890));

		boolean result = repo.delete(created);
		Assert.assertTrue(result);
	}
}
